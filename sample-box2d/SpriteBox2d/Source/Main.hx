package;


import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import openfl.Assets;
import box2D.common.*;
import box2D.collision.*;
import box2D.dynamics.*;
import box2D.common.math.*;
import box2D.collision.shapes.*;


class Main extends Sprite {
	
	
	public function new () {
		
		super ();

		var Logo = new Sprite ();
		Logo.addChild (new Bitmap (Assets.getBitmapData ("assets/openfl.png")));
		Logo.x = 100;
		Logo.y = 100;
		addChild (Logo);
		
		// 世界をつくる
		var gravity = new B2Vec2(0.0,10.0);
		var doSleep = true;
		var world = new B2World(gravity, doSleep);

		// 剛体の定義 と 生成
		var bodyDef = new B2BodyDef();
		bodyDef.type = B2Body.b2_dynamicBody; // 動的にする
		bodyDef.position.set(5.0,4.0); // 位置を決める
		var body = world.createBody(bodyDef); // 定義から剛体を生成する

		// 形をつくる
		var dynamicBox = new B2PolygonShape();
		dynamicBox.setAsBox(1.0,1.0); // 幅 1m 高さ 1mの箱をつくる

		// 装備をきめる
		var fixtureDef = new B2FixtureDef();
		fixtureDef.shape = dynamicBox; // 形を決定する
		fixtureDef.density = 1.0; // 密度を決める
		fixtureDef.friction = 0.3; // 摩擦力を決める
		fixtureDef.restitution = 0.7; // 反発係数を決定
		body.createFixture(fixtureDef); // 剛体に装備を付加

		var positionToPixcel = function(position){
			return position * (1/1000);
		};

		var timeStamp = 1.0 / 60.0;
		var velocityIterations = 6;
		var  positionIterations = 2;

		addEventListener(flash.events.Event.ENTER_FRAME,function(e){
			world.step(timeStamp, velocityIterations, positionIterations);
			Logo.x = body.getPosition().x;
			Logo.y = body.getPosition().y;
			});
		
	}
	
	
}